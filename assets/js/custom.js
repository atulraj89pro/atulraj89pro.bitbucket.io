$(document).ready(function() {
    var isFormSubmitted = 0,
        isModalClosed = 0;
    if (document.cookie.split(';').some(function(item) {
            return item.indexOf('formSubmitted=1') >= 0
        })) {
        isFormSubmitted = 1;
    }
    if (document.cookie.split(';').some(function(item) {
            return item.indexOf('modalClosed=1') >= 0
        })) {
        isModalClosed = 1;
    }
    if (isModalClosed == 1 || isFormSubmitted == 1) {
        $('.isCookieSet').show();
        $('.demo').addClass('cookieAdded');
        $('.cookieValue').html("<p>Form Submitted = " + isFormSubmitted + "</p><p> Modal closed = " + isModalClosed + "<p>")
    }
    $('.close').click(function() {
        document.cookie = "modalClosed=1";
        $('.customModal').removeClass('show');
        $('body').removeClass('overflowHidden');
    });
    $(document).on("mouseleave", function(event) {
        if (event.clientY < 0 && isModalClosed == 0 && isFormSubmitted == 0) {
            $('.customModal').addClass('show');
            $('body').addClass('overflowHidden');
        }
    });
    $('#subsForm').on('submit', function() {
        var email = $("#email").val();
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if (email == '' || !email.match(reg)) {
            $("#email").next('p').addClass("show");
            return false;
        } else {
            $("#email").next('p').removeClass("show");
        }
        if ($("#checkMe").prop("checked") == false) {
            $("#checkMe").siblings('.error').addClass("show");
            return false;
        }
        // if submitted set cookie before close
        document.cookie = "formSubmitted=1";
        document.cookie = "modalClosed=1";
    });

    // clear cookie button just for testing purpose;
    $('.clearCookie').click(function() {
        document.cookie = "formSubmitted=0";
        document.cookie = "modalClosed=0";
        window.location.reload();
    });

    /*check if the device is mobile or window width is less than 1025,
    because 1024 is the resolution for iPad landscape*/
    var isMobile = false;
    window.mobileCheck = function() {
        (function(a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) isMobile = true; })(navigator.userAgent || navigator.vendor || window.opera);
        return isMobile;
    };

    var windowWidth = $(window).width();
    // this if Condition is for demo purpose
    if ((isMobile || windowWidth < 1025)) {
        // alert('mobile');
        $('h2.demoText').text('This is a Mobile Device now. or Browser Window is Less than 1025, Wait for 5 Seconds');
        $('p.demoText').hide();
    } else {
        $('h2.demoText').text('Try to move mouse to the top part of the Window');
        $('p.demoText').show();
    }

    if ((isMobile || windowWidth < 1025) && (isModalClosed == 0 && isFormSubmitted == 0)) {
        setTimeout(function() {
            $('.customModal').addClass('show');
        }, 5000);
    }

});